# **SP1-snake**

Snake game in a terminal for a school project.  
I decided that I'll make a snake game for a school project even though I don't have to. It will be just for bonus point (I hope) and to learn more about 
[Rust](https://www.rust-lang.org).  
I already know how to program in C# and some C/C++. So I hope that using a new programming language won't be a problem.

## **Rust crates used**
The contents of `Cargo.toml`
```toml
[dependencies]
rand = "0.3.15"
rustbox = "0.9.0"
fps_clock = "1.0"
```
## **What I'm trying to figure out now**
* How to change the font size to be square
  * I don't know if it is possible