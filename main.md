# **Main**
```rust
extern crate fps_clock;
extern crate rand;
extern crate rustbox;

use rand::*;

use rustbox::{Color, Key, RustBox};

fn main() {
    let mut rng = rand::thread_rng();
    let mut a: f64;
    loop {
        a = rng.gen();
        println!("{}", a * 100.0);
    }
}
```