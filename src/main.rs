extern crate fps_clock;
extern crate rand;
extern crate rustbox;
extern crate snake;
use fps_clock::FpsClock;
use rand::Rng;
use rustbox::{Color, Key, RustBox};
use snake::{apple, direction, player, point};
use std::default::Default;
use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::time::Duration;

fn show_intro(rb: &RustBox) {
    let ascii_art = vec![
        " .----------------.  .-----------------. .----------------.  .----------------.  .----------------. "
        "| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |"
        "| |    _______   | || | ____  _____  | || |      __      | || |  ___  ____   | || |  _________   | |"
        "| |   /  ___  |  | || ||_   \|_   _| | || |     /  \     | || | |_  ||_  _|  | || | |_   ___  |  | |"
        "| |  |  (__ \_|  | || |  |   \ | |   | || |    / /\ \    | || |   | |_/ /    | || |   | |_  \_|  | |"
        "| |   '.___`-.   | || |  | |\ \| |   | || |   / ____ \   | || |   |  __'.    | || |   |  _|  _   | |"
        "| |  |`\____) |  | || | _| |_\   |_  | || | _/ /    \ \_ | || |  _| |  \ \_  | || |  _| |___/ |  | |"
        "| |  |_______.'  | || ||_____|\____| | || ||____|  |____|| || | |____||____| | || | |_________|  | |"
        "| |              | || |              | || |              | || |              | || |              | |"
        "| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |"
        " '----------------'  '----------------'  '----------------'  '----------------'  '----------------' "
    ];

    let art_width =   ascii_art[0].len() as i16;
    let art_height =  ascii_art.len() as i16;
    let term_width =  rb.width() as i16;
    let term_height = rb.height() as i16;
    let start_x = ((term_width - art_width)/2) as i16;
    let start_y = ((term_height - art_height)/2) as i16;

    for (i,y) in (start_y..(start_y+art_height)).enumerate() {
        rb.print(start_x as usize, y as usize, rustbox::RB_BOLD, Color::Green, Color::Default, ascii_art[i]);
    };

    let next_key_message = "Hit space to continue.";
    let quit_message = "Hit 'esc' to exit.";

    rb.print(((term_width - next_key_message.len() as i16)/2) as i16, (term_height-2) as usize, rustbox::RB_BOLD, Color::Default, Color::Default, next_key_message);
    rb.print(((term_width - quit_message.len()as i16)/2) as usize, (term_height-1)as usize, rustbox::RB_BOLD, Color::Default, Color::Default, quit_message);

    loop {
        rb.present();

        match rb.poll_event(false) {
            Ok(rustbox::Event::KeyEvent(key)) => {
                match key {
                    Key::Char(' ') => { break; },
                    Key::Esc => { panic!(); },
                    _ => {}
                }
            },
            Err(e) => panic!("{}", e),
            _ => {}
        }
    }
}

fn main() {
    
}
